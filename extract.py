#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
--------------------------------------------
Individual fruit extraction

@author kyon <kyon.0308@gmail.com>
@created 2015/02/05
--------------------------------------------
"""

import cv2
import numpy as np


if __name__=='__main__':

    #read image
    org_img = cv2.imread('org.png')
    xm_img = cv2.imread('xm.png', 0)
    
    #thresholding (this is necessary if the image is not binary)
    _, xm_img = cv2.threshold(xm_img, 127, 255, cv2.THRESH_BINARY)

    #remove small areas (if necessary)
    xm_img2 = cv2.morphologyEx(xm_img, cv2.MORPH_OPEN, np.ones((3, 3), dtype=int), iterations=1)
    
    #labeling
    contours, _ = cv2.findContours(xm_img2.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    #process blob by blob
    for contour in contours:
        #extract xy coordinates of individual fruit
        #(the center of the gravity of each blobs)
        center_x = contour[:, 0, 0].mean()
        center_y = contour[:, 0, 1].mean()
        center = (int(center_x), int(center_y))

        #draw individual fruit position
        cv2.circle(org_img, center, radius=20, color=(255, 0, 0), thickness=3)

    #save result image
    cv2.imwrite('result.png', org_img)
